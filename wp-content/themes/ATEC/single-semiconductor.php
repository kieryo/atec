<?php get_header(); ?>
<?php global $post; $post_id = $post->ID; ?>

<?php
	$fields = get_fields();
	if( !empty($fields['banner_image']) ) {

		// POST HAS BANNER IMAGE, USE BANNER IMAGE
		$banner_image = $fields['banner_image']['url'];
		$banner_text = $fields['banner_text'];

	} else {

		// POST NO BANNER IMAGE, USE BANNER IMAGE OF MEDIA
	  // Get Banner Image from Media Page 
	  $args = array(
	  	'post_type' => 'page',
	  	'page_id'  => 370,
	  );
	  $query = new wp_query($args);

		if($query->have_posts()) : 
			while ( $query->have_posts() ) : $query->the_post(); $fields_line = get_fields(); 
				$banner_image = $fields_line['banner_image']['url'];
				$banner_text = $fields_line['banner_text'];
			endwhile; 
		endif; 
		wp_reset_postdata(); 

	}

?>

<?php if(!empty($banner_image)): ?>

<?php 
	$taxonomy = get_the_terms( $post->ID, 'semiconductor_type' );
?>

	<section class="inside-pages-banner" style="background-image: url(<?= $banner_image; ?>);">
		<h2 class="banner-title"><?= $banner_text; ?></h2>
		<?php if($taxonomy[0]): ?>
			<p class="banner-desc"><?= $taxonomy[0]->name; ?></p>
		<?php endif; ?>
		<div class="shadow"></div>
	</section>
<?php endif; ?>

<section class="atec-wrapper semiconductor-article-page">
	<div class="quicklinks">

		<?php
		$custom_terms = get_terms('semiconductor_type');

		foreach($custom_terms as $custom_term) {
		    wp_reset_query();
		    $args = array('post_type' => 'semiconductor',
		        'tax_query' => array(
		            array(
		            		'orderby' => 'desc',
		                'taxonomy' => 'semiconductor_type',
		                'field' => 'slug',
		                'terms' => $custom_term->slug,
		            ),
		        ),
		     );


		     $loop = new WP_Query($args);
		     if($loop->have_posts()) { ?>
					<div class="semiconductor-types-wrapper">
							<div class="title-wrapper">
								<h2 class="title"><?= $custom_term->name; ?></h2>
							</div>
			     		<div class="articles">
					     	<?php while($loop->have_posts()) : $loop->the_post(); ?>
						     		<a href="<?= the_permalink(); ?>" class="semiconductor-types-articles <?= ($post_id == get_the_ID()) ? 'active' : '' ; ?>">
											<h3 class="title"><?= the_title(); ?></h3>
						     		</a>
					      <?php endwhile; ?>
			     		</div>
		     </div>
		   <?php } ?>
		<?php } ?>		
		<?php wp_reset_postdata(); ?>
	</div>
	<div class="main-article">
		<h2 class="title"><?= the_title(); ?></h2>
		<?= the_content(); ?>

		<?php if(isset($fields['files']) && $fields['files'] != ''): ?>
			<div class="downloads-mainwrapper mt-5">
				<h3 class="subtitle">Downloads</h3>

				<div class="dl-wrapper mt-4">
					<?php foreach($fields['files'] as $val): ?>
						<a href="<?= $val['file']['url']; ?>" target="_blank" class="downloads-wrapper">
							<p class="dl-title"><?= $val['filename']; ?></p>
							<i class="fa fa-file-download"></i>
						</a>
					<?php endforeach; ?>
				</div>

			</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>