<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
						$fields = get_fields();
					endwhile;
				endif;
			?>
		</div>

		<?php if(!empty($fields['certifications'])): ?>
			
			<div class="content-wrapper certifications-wrapper">

				<div class="row">
					<?php foreach ($fields['certifications'] as $key => $value) { ?>
						<div class="col-md-4">
							<a href="<?= $value['pdf_file']['url']; ?>" class="cert-container" target="_blank">
								<img src="<?= $value['logo']['url']; ?>" alt="<?= $value['logo']['alt']; ?>">
								<p><?= nl2br($value['description']); ?></p>
							</a>
						</div>
					<?php } ?>
				</div>

			</div>

		<?php endif; ?>
	</div>

</section>

<?php get_footer(); ?>