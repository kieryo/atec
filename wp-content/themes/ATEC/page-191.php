<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages contact-us-container">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<div class="row">
				<div class="col-md-12 text-center mb-3">
					<h2>Get in Touch</h2>
				</div>
				<div class="left-content col-md-12">
					<?php
						if( have_posts() ) :
							while( have_posts() ): the_post();
								the_content();
								$fields = get_fields();
							endwhile;
						endif;
					?>
				</div>
				<div class="right-content col-md-12">
					<?php if($fields) : ?>
						<!-- GOOGLE MAP -->
						<?php if($fields['embed_google_map']): ?>
							<div class="gmap-wrapper">
								<?= $fields['embed_google_map']; ?>
							</div>
						<?php endif; ?>
						<!-- CONTACT DETAILS -->
						<?php if($fields['contact_details']) : ?>
							<div class="contact-details">
								<?php foreach($fields['contact_details'] as $key => $val) { ?>
									<div class="contact-details-wrapper">
										<div class="row">
											<div class="contact-title col-md-4">
												<p><?= $val['title']; ?></p>
											</div>
											<div class="contact-desc col-md-8">
												<?= $val['description']; ?>
											</div>
										</div>
									</div>
								<?php } ?>

							</div>
						<?php endif; ?>

				<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</section>


<?php get_footer(); ?>