<?php $fields = get_fields(); ?>
<?php //print'<pre>';print_r($fields);print'</pre>'; ?>

<section class="content-wrapper quote-wrapper">
	<div class="atec-inner-container">
		<div class="quote-icon top">
			<div class="quote"></div>
			<div class="quote"></div>
		</div>
		<h3 class="quote-title"><?= $fields['quote']; ?></h3>
		<div class="quote-icon bottom">
			<div class="quote"></div>
			<div class="quote"></div>
		</div>
	</div>
</section>


<section class="article-wrapper">
	<div class="atec-inner-container">
		<div class="row articles">


			<?php

				$cpt_arg = array(
					'post_type' => 'connectivity', 
					'post_status' => 'publish', 
			  	'posts_per_page' => -1,
			  	'order_by' => 'date',
					'order' => 'DESC'
				);
			  
			  $cpt_query = new WP_Query($cpt_arg);

			?>
			<?php if ($cpt_query->have_posts()) :  ?>
				<?php while ($cpt_query->have_posts()) : $cpt_query->the_post();  ?>
					<a href="<?= the_permalink(); ?>" class="semiconductor-types-articles col-md-4">
						<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
							<?php the_post_thumbnail('full'); ?>
						<?php } ?> 														  		
						<h3 class="title"><?= the_title(); ?></h3>
						<p><?= the_excerpt(); ?></p>
						<p class="know-more">Know More</p>
					</a>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>