<?php if(have_posts() ) : ?>
	<?php while( have_posts() ) : the_post(); ?>
		<?php $fields = get_fields(); ?>
		<?php if(isset($fields['banner_video']) && $fields['banner_video'] != ''){ ?>
			<section class="inside-pages-banner has-video-banner">
        <video autoplay muted loop class="d-block w-100">
          <source src="<?= $fields['banner_video']; ?>" type="video/mp4">
          <!-- <source src="/web/20160807090647im_/http://www.spi-global.com/sites/all/themes/spiglobal/images/globalbannervid.webm" type="video/webm"> -->
        </video>
				<h2 class="banner-title"><?= $fields['banner_text']; ?></h2>
				<div class="shadow"></div>
			</section>
		<?php } else { ?>
			<section class="inside-pages-banner" style="background-image: url(<?= $fields['banner_image']['url'] ?>);">
				<h2 class="banner-title"><?= $fields['banner_text']; ?></h2>
				<div class="shadow"></div>
			</section>
		<?php } ?>
	<?php endwhile; ?>
<?php endif; ?>