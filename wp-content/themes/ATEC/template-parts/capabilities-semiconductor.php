<?php $fields = get_fields(); ?>
<?php //print'<pre>';print_r($fields);print'</pre>'; ?>

<section class="content-wrapper quote-wrapper">
	<div class="atec-inner-container">
		<!-- <div class="quote-icon top">
			<div class="quote"></div>
			<div class="quote"></div>
		</div> -->
		<h3 class="quote-title"><?= $fields['quote']; ?></h3>
		<!-- <div class="quote-icon bottom">
			<div class="quote"></div>
			<div class="quote"></div>
		</div> -->
	</div>
</section>


<section class="content-wrapper semiconductor">
	<div class="atec-inner-container">
		<?php
		$custom_terms = get_terms('semiconductor_type');

		foreach($custom_terms as $custom_term) {
		    wp_reset_query();
		    $args = array('post_type' => 'semiconductor',
		        'tax_query' => array(
		            array(
		            		'orderby' => 'desc',
		                'taxonomy' => 'semiconductor_type',
		                'field' => 'slug',
		                'terms' => $custom_term->slug,
		            ),
		        ),
		     );


		     $loop = new WP_Query($args);
		     if($loop->have_posts()) { ?>
					<div class="semiconductor-types-wrapper">
							<div class="title-wrapper">
								<h2 class="title"><?= $custom_term->name; ?></h2>
								<p class="desc"><?= term_description($custom_term->term_id, $custom_terms); ?></p>
							</div>
			     		<div class="row articles">
					     	<?php while($loop->have_posts()) : $loop->the_post(); ?>
						     		<a href="<?= the_permalink(); ?>" class="semiconductor-types-articles col-md-4">
											<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
												<?php the_post_thumbnail('full'); ?>
											<?php } ?>  		
											<h3 class="title"><?= the_title(); ?></h3>
											<?= the_excerpt(); ?>
											<p class="know-more">Know More</p>
						     		</a>
					      <?php endwhile; ?>
			     		</div>
		     </div>
		   <?php } ?>
		<?php } ?>
	</div>
</section>