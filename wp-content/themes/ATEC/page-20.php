<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
					endwhile;
				endif;
			?>
		</div>



		<?php 
			/* DISPLAY MARKETS */
		  $args = array(
		  	'post_type'=> 'management',
		  	'posts_per_page' => -1,
		  	'order'    => 'DESC',
		  	'post_status' => 'publish'
		  );
		  query_posts($args);
	  ?>
		<?php if(have_posts()) : ?>
			<div class="content-wrapper mangement-wrapper">
				<div class="row">
					
					<?php while(have_posts()) : the_post(); ?>
						<a href="#" data-toggle="modal" data-target="#management<?= get_the_ID(); ?>" class="management-inner col-md-4">
							<div class="img-wrapper">
								<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
									<?php the_post_thumbnail('full'); ?>
								<?php } ?> 
							</div>
							<h3 class="management-name"><?= the_title(); ?></h3>
							<p class="position"><?= get_fields()['position']; ?></p>
						</a>

						<div class="modal fade management-modal" id="management<?= get_the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="management-<?= get_the_ID(); ?>-Label" aria-hidden="true">
						  <div class="modal-dialog modal-dialog-centered" role="document">
						    <div class="modal-content">
						      <div class="modal-body">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						        <div class="img-wrapper">
											<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
												<?php the_post_thumbnail('full'); ?>
											<?php } ?> 
						        </div>
						        <div class="management-info-wrapper">
											<h3 class="management-name"><?= the_title(); ?></h3>
											<p class="position"><?= get_fields()['position']; ?></p>
						        	<div class="management-info">
								        <?= the_content(); ?>
							        </div>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>

					<?php endwhile; ?>

				</div>
			</div>
		<?php endif; ?>
	</div>

</section>

<?php get_footer(); ?>