<?php 

// Creating the widget 
class atec_footer_widget extends WP_Widget {
	  
	function __construct() {
		parent::__construct(
		  
			// Base ID of your widget
			'atec_footer_widget', 
			  
			// Widget name will appear in UI
			__('Footer Details Widget', 'atec_footer_widget_domain'), 
			  
			// Widget description
			array( 'description' => __( 'Footer Details Widget', 'atec_footer_widget_domain' ), ) 
		);
	}
	  
	// Creating widget front-end
	  
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$description = apply_filters( 'widget_description', $instance['description'] );
		  
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		?>
		<div class="footer-details-widget">
			<?php if(!empty($title)) : ?>
				<h4 class="footer-title"><?= $title; ?></h4>
			<?php endif; ?>
			<?php if(!empty($description)) : ?>
				<p><?= nl2br($description); ?></p>
			<?php endif; ?>
		</div>
		<?php
		echo $args['after_widget'];
	}
	          
	// Widget Backend 
	public function form( $instance ) {

		$title =  ( isset($instance['title']) ) ? $instance[ 'title' ] : '';
		$description = ( isset($instance['description'])) ? $instance['description'] : '';
		// Widget admin form
		?>
		<div class="form-group" style="margin: 15px 0;">
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><strong><?php _e( 'Title:' ); ?></strong></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</div>
		<div class="form-group">
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><strong><?php _e( 'Description:' ); ?></strong></label> 
			<textarea class="widefat" rows="2" cols="20" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>"><?php echo $description; ?></textarea>
		</div>
		<?php 
	}
	      
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		return $instance;
	}
}
 
// Register and load the widget
function atec_load_widget() {
    register_widget( 'atec_footer_widget' );
}
add_action( 'widgets_init', 'atec_load_widget' );


