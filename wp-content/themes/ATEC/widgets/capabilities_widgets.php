<?php 

// Creating the widget 
class atec_capabilities extends WP_Widget {
	  
	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'atec_capabilities', 
			// Widget name will appear in UI
			__('Capabilities', 'atec_capabilities_domain'), 
			// Widget description
			array( 'description' => __( 'Capabilities Used in Front Page', 'atec_capabilities_domain' ), ) 
		);
	}
	  
	// Creating widget front-end
	public function widget( $args, $instance ) {
		$left_title = apply_filters( 'widget_left_title', $instance['left_title'] );
		$left_text = apply_filters( 'widget_left_text', $instance['left_text'] );
		$left_button_text = apply_filters( 'widget_left_button_text', $instance['left_button_text'] );
		$left_button_link = apply_filters( 'widget_left_button_link', $instance['left_button_link'] );

		$right_title = apply_filters( 'widget_right_title', $instance['right_title'] );
		$right_text = apply_filters( 'widget_right_text', $instance['right_text'] );
		$right_button_text = apply_filters( 'right_button_text', $instance['right_button_text'] );
		$right_button_link = apply_filters( 'widget_right_button_link', $instance['right_button_link'] );
		  
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		// HTML to display
		?>
		<section class="capabilities-wrapper">
			<div class="row">
				<div class="capabilities-container col-md-6">
					<?php if( !empty($instance["left_image_uri"])) { ?>
						<img src="<?php echo esc_url($instance["left_image_uri"]); ?>" />
					<?php } ?>
					<h3><?php if ( ! empty( $left_title ) ) echo $args['before_title'] . $left_title . $args['after_title']; ?></h3>
					<p><?php if ( ! empty( $left_text ) ) echo $left_text; ?></p>
					<a href="<?php echo $left_button_link ?>" class="btn-link white"><?php echo $left_button_text ?></a>
				</div>
				<div class="capabilities-container col-md-6">
					<?php if( !empty($instance["right_image_uri"])) { ?>
						<img src="<?php echo esc_url($instance["right_image_uri"]); ?>" />
					<?php } ?>
					<h3><?php if ( ! empty( $right_title ) ) echo $args['before_title'] . $right_title . $args['after_title']; ?></h3>
					<p><?php if ( ! empty( $right_text ) ) echo $right_text; ?></p>
					<a href="<?php echo $right_button_link ?>" class="btn-link"><?php echo $right_button_text ?></a>
				</div>
			</div>
		</section>
		<?php
		// echo '<img src="'.esc_url($instance["image_uri"]).'" />';
	 //  echo $text;

		echo $args['after_widget'];
	}
	          
	// Widget Backend 
	public function form( $instance ) {

		// $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$left_title = esc_attr( $instance['left_title'] ); 
    $left_text = esc_textarea( $instance['left_text'] );
    $left_button_text = esc_attr( $instance['left_button_text'] );
    $left_button_link = esc_attr( $instance['left_button_link'] );

		$right_title = esc_attr( $instance['right_title'] ); 
    $right_text = esc_textarea( $instance['right_text'] );
    $right_button_text = esc_attr( $instance['right_button_text'] );
    $right_button_link = esc_attr( $instance['right_button_link'] );
		// Widget admin form
		?>
		<div class="capabilities-form" style="margin: 15px 0;">
			<label style="font-size: 14px;font-weight: bold;margin-bottom: 5px;display: inline-block;">Left Content</label>
			<div class="left-content" style="padding: 15px;border: 1px solid #CECECE">
				<div class="form-group" style="margin-bottom: 15px;">
		      <label for="<?= $this->get_field_id( 'left_image_uri' ); ?>"><strong>Image</strong></label>
		      <img class="<?= $this->id ?>_left_img" src="<?= (!empty($instance['left_image_uri'])) ? $instance['left_image_uri'] : ''; ?>" style="margin:0;padding:0;max-width:100%;display:block"/>
		      <input type="hidden" class="widefat <?= $this->id ?>_left_url" name="<?= $this->get_field_name( 'left_image_uri' ); ?>" value="<?= $instance['left_image_uri']; ?>" style="margin-top:5px;" />
		      <input type="button" id="<?= $this->id.'_left' ?>" class="button button-primary js_custom_upload_media" value="Select Image" style="margin-top:5px;"/>
	      </div>
	      <div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'left_title' ); ?>"><strong><?php _e( 'Title' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'left_title' ); ?>" name="<?php echo $this->get_field_name( 'left_title' ); ?>" type="text" value="<?php echo esc_attr( $left_title ); ?>"/>
				</div>
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'left_text' ); ?>"><strong><?php _e( 'Content' ); ?></strong></label> 
					<textarea class="widefat" rows="2" cols="20" id="<?php echo $this->get_field_id('left_text'); ?>" name="<?php echo $this->get_field_name('left_text'); ?>"><?php echo $left_text; ?></textarea>
				</div>
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'left_button_text' ); ?>"><strong><?php _e( 'Button Text' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'left_button_text' ); ?>" name="<?php echo $this->get_field_name( 'left_button_text' ); ?>" type="text" value="<?php echo esc_attr( $left_button_text ); ?>"/>
				</div>
				<div class="form-group">
					<label for="<?php echo $this->get_field_id( 'left_button_link' ); ?>"><strong><?php _e( 'Button Link' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'left_button_link' ); ?>" name="<?php echo $this->get_field_name( 'left_button_link' ); ?>" type="text" value="<?php echo esc_attr( $left_button_link ); ?>"/>
				</div>
			</div>
			<label style="font-size: 14px;font-weight: bold;margin: 15px 0 5px;display: inline-block;">Right Content</label>
			<div class="left-content" style="padding: 15px;border: 1px solid #CECECE">
				<div class="form-group" style="margin-bottom: 15px;">
		      <label for="<?= $this->get_field_id( 'right_image_uri' ); ?>"><strong>Image</strong></label>
		      <img class="<?= $this->id ?>_right_img" src="<?= (!empty($instance['right_image_uri'])) ? $instance['right_image_uri'] : ''; ?>" style="margin:0;padding:0;max-width:100%;display:block"/>
		      <input type="hidden" class="widefat <?= $this->id ?>_right_url" name="<?= $this->get_field_name( 'right_image_uri' ); ?>" value="<?= $instance['right_image_uri']; ?>" style="margin-top:5px;" />
		      <input type="button" id="<?= $this->id.'_right' ?>" class="button button-primary js_custom_upload_media" value="Select Image" style="margin-top:5px;"/>
	      </div>
	      <div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'right_title' ); ?>"><strong><?php _e( 'Title' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'right_title' ); ?>" name="<?php echo $this->get_field_name( 'right_title' ); ?>" type="text" value="<?php echo esc_attr( $right_title ); ?>"/>
				</div>
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'right_text' ); ?>"><strong><?php _e( 'Content' ); ?></strong></label> 
					<textarea class="widefat" rows="2" cols="20" id="<?php echo $this->get_field_id('right_text'); ?>" name="<?php echo $this->get_field_name('right_text'); ?>"><?php echo $right_text; ?></textarea>
				</div>
				<div class="form-group" style="margin-bottom: 15px;">
					<label for="<?php echo $this->get_field_id( 'right_button_text' ); ?>"><strong><?php _e( 'Button Text' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'right_button_text' ); ?>" name="<?php echo $this->get_field_name( 'right_button_text' ); ?>" type="text" value="<?php echo esc_attr( $right_button_text ); ?>"/>
				</div>
				<div class="form-group">
					<label for="<?php echo $this->get_field_id( 'right_button_link' ); ?>"><strong><?php _e( 'Button Link' ); ?></strong></label> 
					<input class="widefat" id="<?php echo $this->get_field_id( 'right_button_link' ); ?>" name="<?php echo $this->get_field_name( 'right_button_link' ); ?>" type="text" value="<?php echo esc_attr( $right_button_link ); ?>"/>
				</div>
			</div>
		</div>
		<?php 
	}
	      
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
    $instance['left_image_uri'] = strip_tags( $new_instance['left_image_uri'] );
		$instance['left_title'] = ( ! empty( $new_instance['left_title'] ) ) ? strip_tags( $new_instance['left_title'] ) : '';
		$instance['left_text'] =  ( ! empty( $new_instance['left_text'] ) ) ? strip_tags( $new_instance['left_text'] ) : '';		
		$instance['left_button_text'] =  ( ! empty( $new_instance['left_button_text'] ) ) ? strip_tags( $new_instance['left_button_text'] ) : '';		
		$instance['left_button_link'] =  ( ! empty( $new_instance['left_button_link'] ) ) ? strip_tags( $new_instance['left_button_link'] ) : '';		

    $instance['right_image_uri'] = strip_tags( $new_instance['right_image_uri'] );
		$instance['right_title'] = ( ! empty( $new_instance['right_title'] ) ) ? strip_tags( $new_instance['right_title'] ) : '';
		$instance['right_text'] =  ( ! empty( $new_instance['right_text'] ) ) ? strip_tags( $new_instance['right_text'] ) : '';		
		$instance['right_button_text'] =  ( ! empty( $new_instance['right_button_text'] ) ) ? strip_tags( $new_instance['right_button_text'] ) : '';		
		$instance['right_button_link'] =  ( ! empty( $new_instance['right_button_link'] ) ) ? strip_tags( $new_instance['right_button_link'] ) : '';	

		return $instance;
	}
	// Class atec_capabilities ends here
} 
 
// Register and load the widget

function atec_load_widget() {
    register_widget( 'atec_capabilities' );
}
add_action( 'widgets_init', 'atec_load_widget' );


