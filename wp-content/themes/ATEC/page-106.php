<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->


<section class="main-content inside-pages">
	<div class="atec-inner-container">
		
		<?php
			if( have_posts() ) :
				while( have_posts() ): the_post();
					the_content();
				endwhile;
			endif;
		?>
		
		<div class="content-wrapper">
			<div class="content-title-wrapper">
				<h2 class="page-title">Latest Updates</h2>
			</div>

			<div class="content-body">

				<?php
					/* DISPLAY NEWS */
				  $args = array(
				  	'post_type'=> 'news',
				  	'posts_per_page' => 3,
				  	'order'    => 'DESC'
				  );
				  query_posts($args);
				  $ctr = 0;
				  $indicators = '';
				?>
				<div id="latestUpdates" class="carousel slide" data-ride="carousel">
					<?php if ( have_posts() ) { ?>
					  <div class="carousel-inner">
					  	<?php while( have_posts() ) {  the_post(); $field_carousel = get_fields(); ?>

					  		<?php $active = ($ctr == 0) ? 'active' : ''; ?>
						    <div class="carousel-item <?= $active ?>">
						    	<div class="latest-update-wrapper">
							    	<div class="left-content img-wrapper">
							    		<?php if(isset($field_carousel['video_link']) && $field_carousel['video_link'] != ''){ ?>
												<?php if(strpos($field_carousel['video_link'], 'youtube') !== false) { ?>
											    <a data-fancybox href="<?= $field_carousel['video_link']; ?>" class="data-fancybox-class">
												<?php } else { ?>
									    		<a href="<?= $field_carousel['video_link']; ?>" class="data-fancybox-class" target="_blank">
												<?php } ?>
											<?php } else { ?>
								    		<a href="<?= the_permalink(); ?>">
											<?php } ?>
													<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
														<?php the_post_thumbnail('full'); ?>
													<?php } else { ?> 
														<img src="<?= get_template_directory_uri().'/assets/images/placeholder.png'; ?>" alt="">
													<?php } ?>
												</a>
							    	</div>
									  <div class="right-content media-content">
									  	<p class="time"><?= the_time('F j, Y'); ?></p>
							    		<?php if(isset($field_carousel['video_link']) && $field_carousel['video_link'] != ''){ ?>
												<?php if(strpos($field_carousel['video_link'], 'youtube') !== false) { ?>
											    <a data-fancybox href="<?= $field_carousel['video_link']; ?>" class="title"><?= the_title(); ?></a>
												<?php } else { ?>
									    		<a href="<?= $field_carousel['video_link']; ?>" class="title" target="_blank"><?= the_title(); ?></a>
												<?php } ?>
											<?php } else { ?>
								    		<a href="<?= the_permalink(); ?>" class="title"><?= the_title(); ?></a>
											<?php } ?>
									    <?= the_excerpt(); ?>
							    		<?php if(isset($field_carousel['video_link']) && $field_carousel['video_link'] != ''){ ?>
												<?php if(strpos($field_carousel['video_link'], 'youtube') !== false) { ?>
											    <a data-fancybox href="<?= $field_carousel['video_link']; ?>" class="btn-learn-more">Read More</a>
												<?php } else { ?>
									    		<a href="<?= $field_carousel['video_link']; ?>" class="btn-learn-more" target="_blank">Read More</a>
												<?php } ?>
											<?php } else { ?>
								    		<a href="<?= the_permalink(); ?>" class="btn-learn-more">Read More</a>
											<?php } ?>
									  </div>
								  </div>
						    </div>
								<?php $indicators .= '<li data-target="#latestUpdates" data-slide-to="'.$ctr.'" class="'.$active.'">'; ?>
								<?php $ctr++; ?>

						  <?php } ?>
					  </div>
					  <ol class="carousel-indicators">
					  	<?= $indicators; ?>
					  </ol>
					<?php } ?>
				</div>
			</div>
		</div>


		<div class="content-wrapper other-news">
			<div class="content-title-wrapper">
				<h2 class="page-title">Other News</h2>
			</div>

			<div class="content-body">
				<?php 
					/* DISPLAY OTHER NEWS */
				  $args = array(
				  	'post_type'=> 'news',
				  	'posts_per_page' => 6,
				  	'offset' => 3,
				  	'order'    => 'DESC'
				  );
				  query_posts($args);
				 ?>
				<div class="row">
					<?php if(have_posts()) { ?>
				  	<?php while(have_posts()) { the_post(); $fields_other = get_fields(); ?>
							<div class="col-md-4">
				    		<?php if(isset($fields_other['video_link']) && $fields_other['video_link'] != ''){ ?>
									<?php if(strpos($fields_other['video_link'], 'youtube') !== false) { ?>
								    <a data-fancybox href="<?= $fields_other['video_link']; ?>" class="other-news-wrapper data-fancybox-class">
									<?php } else { ?>
						    		<a href="<?= $fields_other['video_link']; ?>" class="other-news-wrapper data-fancybox-class" target="_blank">
									<?php } ?>
								<?php } else { ?>
								<a href="<?= the_permalink(); ?>" class="other-news-wrapper">
								<?php } ?>
									<div class="img-wrapper">
											<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
												<?php the_post_thumbnail('full'); ?>
											<?php } else { ?> 
												<img src="<?= get_template_directory_uri().'/assets/images/placeholder.png'; ?>" alt="">
											<?php } ?>
								 	</div>
								 	<div class="other-news-info">
									 	<p class="time"><?= the_time('F j, Y'); ?></p>
									 	<h3 class="other-news-title"><?= the_title(); ?></h3>
									 	<?php the_excerpt(); ?>
									 	<p class="btn-learn-more">Read More</p>
								 	</div>
							 	</a>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				
			</div>

		</div>


	</div>
</section>


<?php get_footer(); ?>