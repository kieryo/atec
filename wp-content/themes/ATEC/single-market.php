<?php get_header(); ?>

<?php

	if( have_posts() ) :
		while ( have_posts() ) :  the_post();
			$fields = get_fields();
		endwhile;
	endif;

	if( !empty($fields['banner_image']) ) {

		// POST HAS BANNER IMAGE, USE BANNER IMAGE
		$banner_image = $fields['banner_image']['url'];
		$banner_text = $fields['banner_text'];

	} else {

		// POST NO BANNER IMAGE, USE BANNER IMAGE OF MEDIA
	  // Get Banner Image from Media Page 
	  $args = array(
	  	'post_type' => 'page',
	  	'page_id'  => 153,
	  );
	  $query = new wp_query($args);

		if($query->have_posts()) : 
			while ( $query->have_posts() ) : $query->the_post(); $fields = get_fields(); 
				$banner_image = $fields['banner_image']['url'];
				$banner_text = $fields['banner_text'];
			endwhile; 
		endif; 
		wp_reset_postdata(); 

	}

?>

<?php if(!empty($banner_image)): ?>
	<section class="inside-pages-banner" style="background-image: url(<?= $banner_image; ?>);">
		<h2 class="banner-title"><?= $banner_text; ?></h2>
		<div class="shadow"></div>
	</section>
<?php endif; ?>


<section class="main-content inside-pages">
	<div class="atec-inner-container">
		<div class="content-wrapper">
			<?php if(have_posts()): ?>
				<?php while ( have_posts() ) : ?>
					<?php the_post(); ?>
					<?= the_content(); ?>
					<?php $fields = get_fields(); ?>
				<?php	endwhile; // end while ?>
			<?php endif; ?>
		</div>
	</div>
		
	<?php if(!empty($fields['quote']['quote_text']) && !empty($fields['quote']['quote_image'])) : ?>
		<div class="content-wrapper quote-wrapper mb-0">
			<div class="quote-text">
				<p><?= nl2br($fields['quote']['quote_text']); ?></p>
			</div>
			<div class="quote-img">
				<img src="<?= $fields['quote']['quote_image']['url']; ?>" alt="<?= $fields['quote']['quote_image']['alt']; ?>">
			</div>
		</div>
	<?php endif; ?>
	
	<!-- Related News -->
<!-- 	<div class="atec-inner-container">
		<div class="content-wrapper related-news">
			<div class="content-title-wrapper">
				<h2 class="page-title">Related News</h2>
			</div>
			<div class="content-body">
				<div class="row">
					<?php
						$orig_post = $post;
						global $post;
						$tags = wp_get_post_tags($post->ID);

						if($tags) { // HAS TAGS
							
							$tag_ids = array();

							foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

							$args=array(
								'tag__in' => $tag_ids,
								'posts_per_page'=>3, // Number of related posts that will be shown.
								'ignore_sticky_posts'=>1,
						  	'post_type' => 'news',
						  	'order'    => 'DESC',
						  	'post_status' => 'publish'
							);
							$my_query = new wp_query( $args );
							if( $my_query->have_posts() ) {
								while( $my_query->have_posts() ) { $my_query->the_post(); ?>
									<div class="related-news-wrapper col-md-4">
										<div class="img-wrapper">
											<?= the_post_thumbnail(); ?>
										</div>
										<div class="related-news-info">
											<p class="time"><?= the_time('F j, Y'); ?></p>
											<h4 class="related-news-title"><?= the_title(); ?></h4>
											<?php the_excerpt(); ?>
											<a href="<?= the_permalink(); ?>" class="btn-learn-more">Learn More</a>
										</div>
									</div>
								<?php }
							}
							wp_reset_query();


						} else { // NO TAGS

							$tags = get_terms('post_tag', array('fields'=>'ids') );
							$args = array(
							  'post_type' => 'news',
							  'posts_per_page' => 3,
						  	'order'    => 'DESC',
						  	'post_status' => 'publish',
							  'tax_query' => array(
							    array(
							      'taxonomy' => 'post_tag',
							      'field' => 'id',
							      'terms' => $tags,
							      'operator' => 'NOT IN',
							    )
							  )
							);
							$untagged = new WP_Query( $args );
							if( $untagged->have_posts() ) {
								while( $untagged->have_posts() ) { $untagged->the_post(); ?>
									<div class="related-news-wrapper col-md-4">
										<div class="img-wrapper">
											<?= the_post_thumbnail(); ?>
										</div>
										<div class="related-news-info">
											<p class="time"><?= the_time('F j, Y'); ?></p>
											<h4 class="related-news-title"><?= the_title(); ?></h4>
											<?php the_excerpt(); ?>
											<a href="<?= the_permalink(); ?>" class="btn-learn-more">Learn More</a>
										</div>
									</div>
								<?php }
							}
							wp_reset_query();

						}
						$post = $orig_post;
					?>
				</div>
			</div>
		</div>
	</div> -->

	<?php if(!empty($fields['contact']['contact_message']) && !empty($fields['contact']['contact_button_text']) && !empty($fields['contact']['contact_button_link'])) : ?>
	<div class="contact-us-markets">
		<div class="atec-inner-container">
			<p class="contact-message"><?= $fields['contact']['contact_message']; ?></p>
			<a href="<?= $fields['contact']['contact_button_link']; ?>" class="btn-link white"><?= $fields['contact']['contact_button_text']; ?></a>
		</div>
	</div>
<?php endif; ?>

</section>

<?php get_footer(); ?>