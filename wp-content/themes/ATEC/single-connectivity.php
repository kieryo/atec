<?php get_header(); ?>
<?php global $post; $post_id = $post->ID; ?>

<?php
	$fields = get_fields();
	if( !empty($fields['banner_image']) ) {

		// POST HAS BANNER IMAGE, USE BANNER IMAGE
		$banner_image = $fields['banner_image']['url'];
		$banner_text = $fields['banner_text'];

	} else {

		// POST NO BANNER IMAGE, USE BANNER IMAGE OF MEDIA
	  // Get Banner Image from Media Page 
	  $args = array(
	  	'post_type' => 'page',
	  	'page_id'  => 398,
	  );
	  $query = new wp_query($args);

		if($query->have_posts()) : 
			while ( $query->have_posts() ) : $query->the_post(); $fields_inside = get_fields(); 
				$banner_image = $fields_inside['banner_image']['url'];
				$banner_text = $fields_inside['banner_text'];
			endwhile; 
		endif; 
		wp_reset_postdata(); 

	}

?>

<?php if(!empty($banner_image)): ?>

<?php 
	$taxonomy = get_the_terms( $post->ID, 'semiconductor_type' );
?>

	<section class="inside-pages-banner" style="background-image: url(<?= $banner_image; ?>);">
		<h2 class="banner-title"><?= $banner_text; ?></h2>
		<?php if($taxonomy[0]): ?>
			<p class="banner-desc"><?= $taxonomy[0]->name; ?></p>
		<?php endif; ?>
		<div class="shadow"></div>
	</section>
<?php endif; ?>

<section class="smic-wrapper semiconductor-article-page">
	<div class="quicklinks">

		<div class="semiconductor-types-wrapper">
   		<div class="articles">

				<?php

					$cpt_arg = array(
						'post_type' => 'connectivity', 
						'post_status' => 'publish', 
				  	'posts_per_page' => -1,
				  	'order_by' => 'date',
						'order' => 'DESC'
					);
				  
				  $cpt_query = new WP_Query($cpt_arg);

				?>
				<?php if ($cpt_query->have_posts()) :  ?>
					<?php while ($cpt_query->have_posts()) : $cpt_query->the_post();  ?>

						<div class="title-wrapper">
							<h2 class="title <?= ($post_id == get_the_ID()) ? 'active' : '' ; ?>"><a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></h2>
						</div>
		     		<!-- <a href="<?= the_permalink(); ?>" class="semiconductor-types-articles">
							<h3 class="title"><?= the_title(); ?></h3>
		     		</a> -->
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
   		</div>
   	</div>

	</div>
	<div class="main-article">
		<h2 class="title"><?= the_title(); ?></h2>
		<?= the_content(); ?>

		<?php if(isset($fields['files']) && $fields['files'] != ''): ?>
			<div class="downloads-mainwrapper mt-5">
				<h3 class="subtitle">Downloads</h3>

				<div class="dl-wrapper mt-4">
					<?php foreach($fields['files'] as $val): ?>
						<a href="<?= $val['file']['url']; ?>" target="_blank" class="downloads-wrapper">
							<p class="dl-title"><?= $val['filename']; ?></p>
							<i class="fa fa-file-download"></i>
						</a>
					<?php endforeach; ?>
				</div>

			</div>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>