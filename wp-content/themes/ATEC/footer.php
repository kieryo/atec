		
	
	<footer class="site-footer">
		<div class="atec-container">
			<div class="row">
				<div class="col-md-3 logo-wrapper">
					<?php 
						if(function_exists('the_custom_logo')) {
							$logo = '';
							$custom_logo_id = get_theme_mod('custom_logo');
							$logo = wp_get_attachment_image_src($custom_logo_id);
						}
					?>
					<img src="<?= $logo[0]; ?>" alt="">
				</div>
				<div class="col-md-2 footer-2">
					<h4 class="footer-title">Navigation</h4>
					<?php 
						wp_nav_menu(
							array (
								'menu' => 'footer',
								'container' => '',
								'theme_location' => 'footer',
							)
						);
					?>
				</div>
				<div class="col-md-4 footer-3">
					<?php if(is_active_sidebar('footer3')): ?>
						<?php dynamic_sidebar('footer3'); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-3 footer-4">
					<?php if(is_active_sidebar('footer4')): ?>
						<?php dynamic_sidebar('footer4'); ?>
					<?php endif; ?>

					<div class="social-media-footer">
						<h4 class="footer-title">Social Media</h4>
						<?php 
							wp_nav_menu(
								array (
									'menu' => 'social_media_link_footer',
									'container' => '',
									'theme_location' => 'social_media_link_footer',
								)
							);
						?>
					</div>
				</div>
			</div>
		</div>
	</footer>


		<?php wp_footer(); ?>
	</body>
</html>