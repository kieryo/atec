$(document).ready(function(){

  $('.file-upload').on('click',function(e){

    var input_name = $(this).attr('href').substring(1);

    $('input[name="'+input_name+'"]').click();

    $('input[name="'+input_name+'"]').change(function(){
      var filename = $(this).val().split('\\').pop();
      $(this).parent().parent().find('#resumeField').val(filename);

    });
  }); 

	$(".year-bar").slick({
	  centerMode: true,
	  // centerPadding: "80px",
	  arrows: false,
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  focusOnSelect: true,
	  asNavFor: ".history-slider"
	});

	$(".history-slider").slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  draggable: false,
	  infinite: false,
	  asNavFor: ".year-bar"
	});
	
  let searchInput = $('.atec_search_input');
  $('.atec_search_button').on('click', e => {
    e.stopPropagation();
    if (searchInput.width() == 0) {
      e.preventDefault();
      searchInput.animate({ width: '150px' }, 300);
      searchInput.focus();
    } else {
      if (!searchInput.val()) {
        e.preventDefault();
        searchInput.animate({ width: '0' }, 300);
      }
    }
  });

  $('.data-fancybox-class').fancybox();

  $('.custom-dropdown .fa').on('click',function(e){

    if($(this).hasClass('menu-active')) {
      $(this).removeClass('menu-active');
      $(this).parent().find('> .custom-dropdown-menu').slideUp();
      $(this).addClass("fa-chevron-up").removeClass("fa-chevron-down");
    } else {
      $(this).addClass('menu-active');
      $(this).parent().find('> .custom-dropdown-menu').slideDown();
      $(this).addClass("fa-chevron-down").removeClass("fa-chevron-up");
    }

  });


});


window.onscroll = function() {
  growShrinkLogo()
};

function growShrinkLogo() {
  var main_header = $('.main-header');
  if (document.body.scrollTop > 5 || document.documentElement.scrollTop > 5) {
    main_header.addClass("scrolled");
  } else {
    main_header.removeClass("scrolled");
  }
}

