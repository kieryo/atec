<?php 

function news_type_custom_taxonomy() {

  $labels = array(
    'name' => _x( 'News Types', 'taxonomy general name' ),
    'singular_name' => _x( 'News Type', 'taxonomy singular name' ),
    'menu_name' => __( 'News Types' ),
  );    

  register_taxonomy('Category',array('news'), array(
    'labels' => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_rest'               => true,
    'show_tagcloud'              => false,
  ));
}
add_action( 'init', 'news_type_custom_taxonomy', 0 );

function atec_cpt_news() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'News' ),
		'singular_name'      => __( 'News' ),
		'add_new'            => __( 'Add New News' ),
		'add_new_item'       => __( 'Add New News' ),
		'edit_item'          => __( 'Edit News' ),
		'new_item'           => __( 'New News' ),
		'all_items'          => __( 'All News' ),
		'view_item'          => __( 'View News' ),
		'search_items'       => __( 'Search News' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom News Post Type',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt','category'),
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'taxonomies' => array('post_tag')
	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'news', $args);
}
add_action( 'init', 'atec_cpt_news' );

function atec_cpt_market() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'Market' ),
		'singular_name'      => __( 'Market' ),
		'add_new'            => __( 'Add New Market' ),
		'add_new_item'       => __( 'Add New Market' ),
		'edit_item'          => __( 'Edit Market' ),
		'new_item'           => __( 'New Market' ),
		'all_items'          => __( 'All Market' ),
		'view_item'          => __( 'View Market' ),
		'search_items'       => __( 'Search Market' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom Markets Post Type',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt','category'),
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'taxonomies' => array('post_tag')
	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'market', $args);
}
add_action( 'init', 'atec_cpt_market' );

function atec_cpt_management() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'Management' ),
		'singular_name'      => __( 'Management' ),
		'add_new'            => __( 'Add New Management' ),
		'add_new_item'       => __( 'Add New Management' ),
		'edit_item'          => __( 'Edit Management' ),
		'new_item'           => __( 'New Management' ),
		'all_items'          => __( 'All Management' ),
		'view_item'          => __( 'View Management' ),
		'search_items'       => __( 'Search Management' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom Managements Post Type',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail',),
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
		'publicly_queryable' => true,  // you should be able to query it
		'show_ui' => true,  // you should be able to edit it in wp-admin
		'exclude_from_search' => true,  // you should exclude it from search results
		'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
		'has_archive' => false,  // it shouldn't have archive page
		'rewrite' => false,  // it shouldn't have rewrite rules
	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'management', $args);
}
add_action( 'init', 'atec_cpt_management' );

function atec_cpt_history() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'History' ),
		'singular_name'      => __( 'History' ),
		'add_new'            => __( 'Add New History' ),
		'add_new_item'       => __( 'Add New History' ),
		'edit_item'          => __( 'Edit History' ),
		'new_item'           => __( 'New History' ),
		'all_items'          => __( 'All History' ),
		'view_item'          => __( 'View History' ),
		'search_items'       => __( 'Search History' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom Historys Post Type',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail',),
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
		'publicly_queryable' => true,  // you should be able to query it
		'show_ui' => true,  // you should be able to edit it in wp-admin
		'exclude_from_search' => true,  // you should exclude it from search results
		'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
		'has_archive' => false,  // it shouldn't have archive page
		'rewrite' => false,  // it shouldn't have rewrite rules
	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'history', $args);
}
add_action( 'init', 'atec_cpt_history' );


// REMOVE POST TYPE
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
}
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
}
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}



// ------------COVID RESPONSONSE CPT ------------

function atec_semiconductor() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'Semiconductor' ),
		'singular_name'      => __( 'Semiconductor' ),
		'add_new'            => __( 'Add New Semiconductor' ),
		'add_new_item'       => __( 'Add New Semiconductor' ),
		'edit_item'          => __( 'Edit Semiconductor' ),
		'new_item'           => __( 'New Semiconductor' ),
		'all_items'          => __( 'All Semiconductor' ),
		'view_item'          => __( 'View Semiconductor' ),
		'search_items'       => __( 'Search Semiconductor' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom Semiconductor',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'menu_icon'         => 'dashicons-welcome-write-blog',
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
		'taxonomies' => array('semiconductor_type'),
    'has_archive' => 'Semiconductor',	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'semiconductor', $args);
}
add_action( 'init', 'atec_semiconductor' );
 
function taxonomy_semiconductor() {
 
  $labels = array(
    'name' => _x( 'Semiconductor', 'taxonomy general name' ),
    'singular_name' => _x( 'Semiconductor', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Semiconductor  Category' ),
    'all_items' => __( 'All Semiconductor' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Semiconductor' ), 
    'update_item' => __( 'Update Semiconductor' ),
    'add_new_item' => __( 'Add New Semiconductor' ),
    'new_item_name' => __( 'New Semiconductor Name' ),
    'menu_name' => __( 'Semiconductor' ),
  ); 	
 
  register_taxonomy('semiconductor_type',array('semiconductor'), array(
    'hierarchical' => true,
    'show_in_rest' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'semiconductor_type' ),
  ));
}
add_action( 'init', 'taxonomy_semiconductor', 0 );




function atec_connectivity() {
	// Set the labels, this variable is used in the $args array
	$labels = array(
		'name'               => __( 'Connectivity' ),
		'singular_name'      => __( 'Connectivity' ),
		'add_new'            => __( 'Add New Connectivity' ),
		'add_new_item'       => __( 'Add New Connectivity' ),
		'edit_item'          => __( 'Edit Connectivity' ),
		'new_item'           => __( 'New Connectivity' ),
		'all_items'          => __( 'All Connectivity' ),
		'view_item'          => __( 'View Connectivity' ),
		'search_items'       => __( 'Search Connectivity' ),
		'featured_image'     => 'Thumbnail',
		'set_featured_image' => 'Add Thumbnail',
	);
	// The arguments for our post type, to be entered as parameter 2 of register_post_type()
	$args = array(
		'labels'            => $labels,
		'description'       => 'Custom Connectivity',
		'public'            => true,
		'menu_position'     => 5,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'menu_icon'         => 'dashicons-welcome-write-blog',
		'has_archive'       => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'show_in_rest'      => true,
		'query_var'         => true,
    'has_archive' => 'Connectivity',	);

	// Call the actual WordPress function
	// Parameter 1 is a name for the post type
	// Parameter 2 is the $args array
	register_post_type( 'connectivity', $args);
}
add_action( 'init', 'atec_connectivity' );