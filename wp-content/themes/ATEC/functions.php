<?php


// Get Menu Walker
require get_template_directory() . '/classes/menu_walker.php';

// Widgets
// require get_template_directory() . '/widgets/capabilities_widgets.php';
require get_template_directory() . '/widgets/footer_info_widgets.php';
require get_template_directory() . '/inc/custom_post_types.php';

// Register Styles
function atec_register_styles() {

	$version = wp_get_theme()->get( 'Version' ); 
	wp_enqueue_style('atec-bootstrap', get_template_directory_uri() . "/assets/libraries/bootstrap/css/bootstrap.min.css", array(), '4.4.1', 'all');
	wp_enqueue_style('atec-slick', get_template_directory_uri() . "/assets/libraries/slick/slick.css", array(), '1.8.1', 'all');
	wp_enqueue_style('atec-slick-theme', get_template_directory_uri() . "/assets/libraries/slick/slick-theme.css", array(), '1.8.1', 'all');
	wp_enqueue_style('atec-fancybox', get_template_directory_uri() . "/assets/libraries/fancybox/dist/jquery.fancybox.min.css", array(), '3.5.7', 'all');
	wp_enqueue_style('atec-fontawesome', "https://use.fontawesome.com/releases/v5.8.2/css/all.css");
	wp_enqueue_style('atec-style', get_template_directory_uri() . "/style.css", array(), $version, 'all');

}
add_action( 'wp_enqueue_scripts', 'atec_register_styles' );

// Register Scripts
function atec_register_scripts() {

	$version = wp_get_theme()->get( 'Version' ); 
	wp_enqueue_script('atec-jquery', get_template_directory_uri() . "/assets/libraries/jquery-3.5.1.min.js" , array(), '3.5.1', true ) ;
	wp_enqueue_script('atec-popper', get_template_directory_uri() . "/assets/libraries/popper.min.js" , array(), '1.16.0', true ) ;
	wp_enqueue_script('atec-bootstrap', get_template_directory_uri() . "/assets/libraries/bootstrap/js/bootstrap.min.js" , array(), '4.4.1', true ) ;
	wp_enqueue_script('atec-slick', get_template_directory_uri() . "/assets/libraries/slick/slick.min.js" , array(), '4.4.1', true ) ;
  wp_enqueue_script('atec-fancybox', get_template_directory_uri() . "/assets/libraries/fancybox/dist/jquery.fancybox.min.js" , array(), '3.5.7', true ) ;
	wp_enqueue_script('atec-global', get_template_directory_uri() . "/assets/js/global.js" , array(), $version, true ) ;

}
add_action( 'wp_enqueue_scripts', 'atec_register_scripts' );

// Enqueue additional admin scripts
function admin_script() {
    wp_enqueue_media();
    wp_enqueue_script('ads_script', get_template_directory_uri() . '/assets/js/widget.js', false, '1.0.0', true);
}
add_action('admin_enqueue_scripts', 'admin_script');

function atec_theme_support() {
	// Adds Dynamic title tag support
	add_theme_support('title-tag');
	add_theme_support('custom-logo');
	add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'atec_theme_support');

// Adding excerpt for page
add_post_type_support( 'page', 'excerpt' );

function atec_menus() {
	$menu_locations = array(
		'header_bottom' => 'Header Menu Bottom',
		'header_top' => 'Header Menu Top',
		'header_inside' => 'Header Inside',
		'social_media_link_sticky' => 'Social Media Links Sticky',
		'social_media_link_footer' => 'Social Media Links Footer',
		'footer' => 'Footer Menu'
	); 
	register_nav_menus($menu_locations);
}
add_action('init', 'atec_menus');


// Widgets Locations
function atec_footer_widgets() {
	register_sidebar(
		array(
			'name' => 'Footer 3',
			'id' => 'footer3',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Footer 4',
			'id' => 'footer4',
			'before_widget' => '<div class="widget-item">',
			'after_widget' => '</div>'
		)
	);
}
add_action('widgets_init', 'atec_footer_widgets');

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');



//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);

function so48515097_cf7_select_values($tag)
{
    if ($tag['basetype'] != 'select') {
        return $tag;
    }

    $values = [];
    $labels = [];
    foreach ($tag['raw_values'] as $raw_value) {
        $raw_value_parts = explode('|', $raw_value);
        if (count($raw_value_parts) >= 2) {
            $values[] = $raw_value_parts[1];
            $labels[] = $raw_value_parts[0];
        } else {
            $values[] = $raw_value;
            $labels[] = $raw_value;
        }
    }
    $tag['values'] = $values;
    $tag['labels'] = $labels;

    // Optional but recommended:
    //    Display labels in mails instead of values
    //    You can still use values using [_raw_tag] instead of [tag]
    $reversed_raw_values = array_map(function ($raw_value) {
        $raw_value_parts = explode('|', $raw_value);
        return implode('|', array_reverse($raw_value_parts));
    }, $tag['raw_values']);
    $tag['pipes'] = new \WPCF7_Pipes($reversed_raw_values);

    return $tag;
}
add_filter('wpcf7_form_tag', 'so48515097_cf7_select_values', 10);