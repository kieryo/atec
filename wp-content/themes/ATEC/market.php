<?php get_header(); ?>

<?php if(have_posts() ) : ?>
	<?php while( have_posts() ) : the_post(); ?>
	<?php $fields = get_fields(); ?>
		<section class="inside-pages-banner" style="background-image: url(<?= $fields['banner_image']['url'] ?>);">
			<h2 class="banner-title"><?= $fields['banner_text']; ?></h2>
			<div class="shadow"></div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<section class="main-content inside-pages">
	<div class="atec-inner-container">
		<?php
			if( have_posts() ) :
				while( have_posts() ): the_post();
					the_content();
				endwhile;
			endif;
		?>
	</div>
</section>

<?php get_footer(); ?>