<?php get_header(); ?>

<?php if(have_posts() ): ?>
	<?php while(have_posts()) : the_post(); ?>
		<?php $fields = get_fields(); ?>
	<?php endwhile; ?>
<?php endif; ?>

<?php $indicator = ''; ?>

<?php if($fields['slider']) { ?>
	<section id="homepageCarousel" class="carousel slide" data-ride="carousel">
		<?php if($fields['persistent_video_link']): ?>
			<div class="persistent-video-wrapper">

				 <?php if(strpos($fields['persistent_video_link'], 'youtube') !== false) { ?>
						<a data-fancybox href="<?= $fields['persistent_video_link']; ?>" class="play-btn"><div class="play-btn-box"></div> Watch Video</a>
					<?php } else { ?>
						<a href="<?= $fields['persistent_video_link']; ?>" class="play-btn" target="_blank"><div class="play-btn-box"></div> Watch Video </a>
					<?php  } ?>
			</div>
		<?php endif; ?>
	  <div class="carousel-inner">
			<?php foreach($fields['slider'] as $key => $val) { ?>
				<?php $class = ($key == 0) ? 'active' : ''; ?>
		    <div class="carousel-item <?= $class; ?>">
		    	<?php if($val['banner_video']) { ?>
		        <video autoplay muted loop class="d-block w-100">
		          <source src="<?= $val['banner_video']; ?>" type="video/mp4">
		          <!-- <source src="/web/20160807090647im_/http://www.spi-global.com/sites/all/themes/spiglobal/images/globalbannervid.webm" type="video/webm"> -->
		        </video>
		    	<?php } else { ?>
			      <img class="d-block w-100" src="<?= $val['banner_image']['url'] ?>" alt="<?= $val['banner_image']['alt'] ?>">
		    	<?php } ?>
				  <div class="carousel-caption">
				    <h5><?= $val['banner_title'] ?></h5>
				    <?php if($val['banner_link']): ?>
						    <a href="<?= $val['banner_link']['url']; ?>" class="white btn-link" target="<?= $val['banner_link']['target']; ?>"><?= $val['banner_link']['title']; ?></a>
				    <?php endif; ?>
				    <?php ///if($val['banner_video']): ?>
							<?php ///if(strpos($val['banner_video_link'], 'youtube') !== false) { ?>
						    <!-- <a data-fancybox href="<?= $val['banner_video_link']; ?>" class="white btn-link">Watch Video</a> -->
						  <?php ///} else { ?>
						    <!-- <a href="<?= $val['banner_video_link']; ?>" class="white btn-link" target="_blank">Watch Video</a> -->
						  <?php ///} ?>
						<?php ///endif; ?>
				  </div>
		    </div>
		    <?php $indicator .= '<li data-target="#homepageCarousel" data-slide-to="'.$key.'" class="'.$class.'"></li>';  ?>
		  <?php } ?>
	  </div>
	  <ol class="carousel-indicators">
		  <?= $indicator; ?>
	  </ol>
	</section>
<?php } ?>

<?php if($fields['capabilities']) { ?>
	<section class="capabilities-wrapper">
		<div class="row">
			<?php foreach($fields['capabilities'] as $key => $val) { ?>
				<div class="capabilities-container col-md-6">
						<figure>
							<img src="<?= $val['icon']['url'] ?>" alt="<?= $val['icon']['alt']; ?>">
						</figure>
						<h3 class="capabilities-title"><?= $val['title'] ?></h3>
						<p><?= $val['description'] ?></p>
						<a href="<?= $val['link']['url']; ?>" class="btn-link <?= ($key == 0) ? 'white' : ''; ?>"><?= $val['link']['title']; ?></a>
				</div>
			<?php } ?>
		</div>
	</section>
<?php } ?>

<?php
	/* DISPLAY NEWS */
  $args = array(
  	'post_type'=> 'news',
  	'posts_per_page' => 3,
  	'order'    => 'DESC',
  	'post_status' => 'publish'
  );
  query_posts($args);
?>

<?php if ( have_posts() ) : ?>

	<section class="section-container news-container">
		<div class="atec-container">
			<div class="section-title-wrapper">
				<h4 class="section-title">News</h4>
			</div>
			<div class="section-content">
				
				<div class="row">
						<?php while ( have_posts() ) : the_post(); $fields_news = get_fields(); ?>
			    		<?php if(isset($fields_news['video_link'])){ ?>
								<?php if(strpos($fields_news['video_link'], 'youtube') !== false) { ?>
							    <a data-fancybox href="<?= $fields_news['video_link']; ?>" class="news-wrapper col-md-4 data-fancybox-class">
								<?php } else { ?>
					    		<a href="<?= $fields_news['video_link']; ?>" class="news-wrapper col-md-4 data-fancybox-class" target="_blank">
								<?php } ?>
							<?php } else { ?>
							<a href="<?= the_permalink(); ?>" class="news-wrapper col-md-4">
							<?php } ?>
								<div class="img-wrapper">
									<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
										<?php the_post_thumbnail('full'); ?>
									<?php } else { ?> 
										<img src="<?= get_template_directory_uri().'/assets/images/placeholder.png'; ?>" alt="">
									<?php } ?>
								</div>
								<p class="time"><?= the_time('F j, Y'); ?></p>
								<h3 class="news-title"><?= the_title(); ?></h3>
							</a>
						<?php endwhile; // end while ?>
						<div class="col-md-12 btn-wrapper text-center">
							<a href="<?= get_permalink(106); ?>" class="btn-link">More News</a>
						</div>

				</div>

			</div>
		</div>
	</section>

<?php endif; ?>
<?php wp_reset_query();  ?>


<?php

	/* DISPLAY MARKETS */
  $args = array(
  	'post_type'=> 'market',
  	'posts_per_page' => 6,
  	'order'    => 'DESC',
  	'post_status' => 'publish',
  	'orderby' => 'date'
  );
  query_posts($args);

?>

<?php if( have_posts() ) : ?>
	<!-- MARKET SECTION -->
	<section class="section-container markets-container">
		<div class="atec-container">
			<div class="section-title-wrapper">
				<h4 class="section-title">Markets</h4>
			</div>
			<div class="section-content">
				
				<div class="row">

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="markets-wrapper col-md-4">
							<div class="markets-inner-wrapper">
								<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
									<?php the_post_thumbnail('full'); ?>
								<?php } ?> 
								<div class="shadow"></div>
								<h3 class="markets-title"><?= the_title(); ?></h3>
								<div class="info-container">
									<h3 class="markets-title"><?= the_title(); ?></h3>
									<?= the_excerpt(); ?>
									<a href="<?= the_permalink(); ?>" class="btn-learn-more white">Learn More</a>
								</div>
							</div>
						</div>
					<?php endwhile; ?>

				</div>

			</div>
		</div>
	</section>
<?php endif; ?>
<?php wp_reset_query();  ?>


<section class="section-container content-wrapper contact-us">
	<div class="atec-container">
		<?php 
			if(have_posts() ): 
				while(have_posts()) : the_post(); 
					the_content(); 
				endwhile; 
			endif; 
		?>
	</div>
</section>




<?php get_footer(); ?>