<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
					endwhile;
				endif;
			?>
		</div>

		<?php 
			/* DISPLAY MARKETS */
		  $args = array(
		  	'post_type'=> 'history',
		  	'posts_per_page' => -1,
		  	'order'    => 'ASC',
		  	'post_status' => 'publish'
		  );
		  query_posts($args);
	  ?>
		<?php if(have_posts()) : ?>
			<div class="content-wrapper history-wrapper">
				<div class="year-bar-wrapper">

		      <div class="year-bar">
						<?php while(have_posts()) : the_post(); ?>
			        <div class="year-bar-bullet"><p><?= the_title(); ?></p></div>
						<?php endwhile; ?>
		      </div>
		      
		      <div class="history-slider">
						<?php while(have_posts()) : the_post(); ?>
			        <div class="history-container">
			        	<div class="history-container-inner">
			        		<div class="left-content">
										<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
											<?php the_post_thumbnail('full'); ?>
										<?php } ?> 
			        		</div>
			        		<div class="right-content">
			        			<h5 class="year"><?= the_title(); ?></h5>
			        			<h4 class="subtitle"><?= get_fields()['subtitle']; ?></h4>
			        			<p><?= the_content(); ?></p>
			        		</div>
		        		</div>
			        </div>
						<?php endwhile; ?>
		      </div>

				</div>
			</div>
		<?php endif; ?>

	</div>

</section>

<?php get_footer(); ?>