<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->


<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
					endwhile;
				endif;
			?>
		</div>

		<?php 
			/* DISPLAY MARKETS */
		  $args = array(
		  	'post_type'=> 'market',
		  	'posts_per_page' => 6,
		  	'order'    => 'DESC',
		  	'post_status' => 'publish'
		  );
		  query_posts($args);
	  ?>
		<?php if(have_posts()) : ?>
			<div class="content-wrapper markets-wrapper">
				<div class="row">
					
					<?php while(have_posts()) : the_post(); ?>
						<div class="markets-inner-wrapper col-md-4">
							<div class="img-wrapper">
								<?php if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?> 
									<?php the_post_thumbnail('full'); ?>
								<?php } ?> 
								<div class="shadow"></div>
							</div>
							<h3 class="markets-title"><?= the_title(); ?></h3>
							<?php the_excerpt(); ?>
							<a href="<?= the_permalink(); ?>" class="btn-learn-more">Learn More</a>
						</div>
					<?php endwhile; ?>

				</div>
			</div>
		<?php endif; ?>

	</div>
</section>


<?php get_footer(); ?>