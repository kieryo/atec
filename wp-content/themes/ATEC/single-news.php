<?php get_header(); ?>

<?php

	if( have_posts() ) :
		while ( have_posts() ) :  the_post();
			$fields = get_fields();
		endwhile;
	endif;

	if( !empty($fields['banner_image']) ) {

		// POST HAS BANNER IMAGE, USE BANNER IMAGE
		$banner_image = $fields['banner_image']['url'];
		$banner_text = $fields['banner_text'];

	} else {

		// POST NO BANNER IMAGE, USE BANNER IMAGE OF MEDIA
	  // Get Banner Image from Media Page 
	  $args = array(
	  	'post_type' => 'page',
	  	'page_id'  => 106,
	  );
	  $query = new wp_query($args);

		if($query->have_posts()) : 
			while ( $query->have_posts() ) : $query->the_post(); $fields = get_fields(); 
				$banner_image = $fields['banner_image']['url'];
				$banner_text = $fields['banner_text'];
			endwhile; 
		endif; 
		wp_reset_postdata(); 

	}

?>

<?php if(!empty($banner_image)): ?>
	<section class="inside-pages-banner" style="background-image: url(<?= $banner_image; ?>);">
		<h2 class="banner-title"><?= $banner_text; ?></h2>
		<div class="shadow"></div>
	</section>
<?php endif; ?>


<section class="main-content inside-pages">
	<div class="atec-inner-container">
		
		<article class="single-news-container">
			<div class="article-wrapper-main">
					<?php if(have_posts()) : ?>
						<?php while ( have_posts() ) : ?>

						<?= the_post(); ?>
						<div class="article-header">
							<div class="article-title-wrapper">
								<h3 class="article-title"><?= the_title(); ?></h3>
							</div>
							<div class="article-sharer">
								<span class="time"><?= the_time('F j, Y'); ?></span>
								<div class="sm-sharer">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_permalink( get_the_ID() ); ?>" class="sm-sharer-sprite fb" target="_blank" onclick="window.open(this.href, 'newwindow', 'width=700, height=400,left=400, top=200'); return false;">Share to Facebook</a>
									<a href="https://twitter.com/intent/tweet?text=<?= get_permalink( get_the_ID() ); ?>" class="sm-sharer-sprite twitter twitter-share-button" target="_blank" onclick="window.open(this.href, 'newwindow', 'width=700, height=400,left=400, top=200'); return false;">Share to Twitter</a>
								</div>
							</div>
						</div>
						<div class="article-body">
							<?= the_content(); ?>
						</div>

					<?php	endwhile; // end while ?>
				<?php endif; ?>
				<?php wp_reset_query();  ?>

			</div>
			<div class="article-sidebar">
				<h4 class="sidebar-title">Related News</h4>
				<ul class="article-list">
					<?php
						$orig_post = $post;
						global $post;
						$tags = wp_get_post_tags($post->ID);

						if($tags) { // HAS TAGS
							
							$tag_ids = array();

							foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

							$args=array(
								'tag__in' => $tag_ids,
								'post__not_in' => array($post->ID),
								'posts_per_page'=>5, // Number of related posts that will be shown.
								'ignore_sticky_posts'=>1,
						  	'post_type' => 'news',
						  	'order'    => 'DESC',
						  	'post_status' => 'publish'
							);
							$my_query = new wp_query( $args );
							if( $my_query->have_posts() ) {
								while( $my_query->have_posts() ) { $my_query->the_post(); ?>
									<li><a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></li>
								<?php }
							}
							wp_reset_query();


						} else { // NO TAGS

							$tags = get_terms('post_tag', array('fields'=>'ids') );
							$args = array(
							  'post_type' => 'news',
							  'posts_per_page' => 5,
						  	'order'    => 'DESC',
						  	'post_status' => 'publish',
							  'tax_query' => array(
							    array(
							      'taxonomy' => 'post_tag',
							      'field' => 'id',
							      'terms' => $tags,
							      'operator' => 'NOT IN',
							    )
							  )
							);
							$untagged = new WP_Query( $args );
							if( $untagged->have_posts() ) {
								while( $untagged->have_posts() ) { $untagged->the_post(); ?>
									<li><a href="<?= the_permalink(); ?>"><?= the_title(); ?></a></li>
								<?php }
							}
							wp_reset_query();

						}
						$post = $orig_post;
					?>
				</ul>

				<a href="<?= get_permalink(106); ?>" class="btn-learn-more">More News</a>
			</div>

		</article>

	</div>
</section>

<?php get_footer(); ?>