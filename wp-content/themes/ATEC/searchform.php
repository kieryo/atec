<form role="search" method="get" id="searchform" class="searchform atec_search" action="<?=esc_url( home_url( '/' ) );?>">
  <input class="atec_search_input" type="text" value="" name="s" id="s" placeholder="Search">
  <label class="atec_search_button" for="searchsubmit"><i class="fas fa-search"></i></label>
  <input class="d-none" type="submit" id="searchsubmit" value="Search">
</form>