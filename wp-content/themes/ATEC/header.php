<!DOCTYPE HTML>

<html lang="en">
	<head>
    <title>ATEC</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		
		<header class="main-header">
			<div class="atec-container">
				<nav class="navbar navbar-expand-md">
					<?php 
						if(function_exists('the_custom_logo')) {
							$logo = '';
							$custom_logo_id = get_theme_mod('custom_logo');
							$logo = wp_get_attachment_image_src($custom_logo_id);
						}
					?>
				  <h1 class="navbar-brand">
				  	<a href="<?= home_url(); ?>"><img id="Logo" src="<?= $logo[0]; ?>" alt=""></a>
				  </h1>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
				    <i class="fa fa-bars"></i>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarMain">
						
						<?php //if(is_front_page()) { ?>
							<div class="navbar-nav navbar-top">
								<?php 
									wp_nav_menu(
										array (
											'menu' => 'header_top',
											'container' => '',
											'theme_location' => 'header_top',
											'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
											'walker' => new custom_sub_walker(),
										)
									);
									get_search_form();
								?>
							</div>
							
							<?php 
								wp_nav_menu(
									array (
										'menu' => 'header_bottom',
										'container' => '',
										'theme_location' => 'header_bottom',
										'items_wrap' => '<ul class="navbar-nav navbar-bottom">%3$s</ul>',
										'walker' => new custom_sub_walker(),
									)
								);
							?>
							<div class="navbar-nav navbar-top mobile-display">
								<?php 
									wp_nav_menu(
										array (
											'menu' => 'header_top',
											'container' => '',
											'theme_location' => 'header_top',
											'items_wrap' => '<ul class="navbar-nav">%3$s</ul>',
											'walker' => new custom_sub_walker(),
										)
									);
									get_search_form();
								?>
							</div>
						<?php //} else { ?>
			<!-- 				
							<div class="header-inside">
								<?php 
									// wp_nav_menu(
									// 	array (
									// 		'menu' => 'header_inside',
									// 		'container' => '',
									// 		'theme_location' => 'header_inside',
									// 		'items_wrap' => '<ul class="navbar-nav navbar-bottom">%3$s</ul>',
									// 		'walker' => new custom_sub_walker(),
									// 	)
									// );
									// get_search_form();
								?>
							</div> -->

						<?php //} ?>
				  </div>
				</nav>
			</div>
			
			<div class="sm-link-sticky-container">
				<?php
					wp_nav_menu(
						array (
							'menu' => 'social_media_link_sticky',
							'container' => '',
							'theme_location' => 'social_media_link_sticky',
							'items_wrap' => '<ul class="sm-links-sticky">%3$s</ul>',
						)
					);
				?>
			</div>

		</header>
