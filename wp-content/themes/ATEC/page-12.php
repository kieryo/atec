<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
						$fields = get_fields();
					endwhile;
				endif;
			?>
		</div>
	</div>
		
	<?php //print'<pre>';print_r($fields);print'</pre>'; ?>
	<?php if($fields['vision_mission']) : ?>
		<div class="content-wrapper vision-mission-wrapper">
			<div class="atec-inner-container">
				<?php foreach($fields['vision_mission'] as $key => $val): ?>
					<div class="vm-wrapper">
						<div class="row">
							<div class="img-wrapper col-md-6">
								<img src="<?= $val['stack_image']['url']; ?>" alt="<?= $val['stack_image']['alt']; ?>">
							</div>
							<div class="content col-md-6">
								<h3 class="vm-title"><?= $val['stack_title']; ?></h3>
								<?= $val['stack_description']; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>


	<?php endif; ?>

	<?php if($fields['message']): ?>

		<div class="content-wrapper message-wrapper">
				
				<div class="row">
					<?php foreach($fields['message'] as $val): ?>
						<div class="msg-wrapper col-md-6">
							<h3 class="msg-title"><?= $val['stack_title']; ?></h3>
							<?= $val['stack_description']; ?>
						</div>
					<?php endforeach; ?>
				</div>

		</div>


	<?php endif; ?>


</section>

<?php get_footer(); ?>