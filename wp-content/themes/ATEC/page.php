<?php get_header(); ?>
<?php global $post; ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
					endwhile;
				endif;
			?>
		</div>
	
	</div>
</section>

<?php
	/* ====================================
					   Capabilities
	=====================================*/ 

	// Semiconductor
  ($post->ID == 370) ? get_template_part('template-parts/capabilities', 'semiconductor') : '';
  // Connectivity
  ($post->ID == 398) ? get_template_part('template-parts/capabilities', 'connectivity') : '';

?>

<?php get_footer(); ?>