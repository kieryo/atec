<?php get_header(); ?>

<?php get_template_part('template-parts/inside', 'banner'); ?> <!-- inside banner -->

<?php $fields = get_fields();?>

<section class="main-content inside-pages">
	<div class="atec-inner-container">

		<div class="content-wrapper">
			<?php
				if( have_posts() ) :
					while( have_posts() ): the_post();
						the_content();
					endwhile;
				endif;
			?>
		</div>
		
		<?php if($fields['jobs']): ?>
			<div class="job-openings-wrapper">
				<div class="title-wrapper">
					<h2 class="title text-center">Job Vacancies</h2>
				</div>
				<div class="body-wrapper">
					<div class="row">
						<?php foreach($fields['jobs'] as $key => $val): ?>
							<div class="jobs-wrapper col-md-4">
								<p class="title"><?= $val['job_name']; ?></p>
								<p class="employment-type"><?= $val['employment_type'] ?></p>
								<div class="description"><?= $val['description']; ?></div>
								<?php if($val['link']): ?>
							    <a href="<?= $val['link']['url']; ?>" class="btn-link" target="<?= $val['link']['target']; ?>"><?= $val['link']['title']; ?></a>
							  <?php endif; ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</div>
</section>


<?php get_footer(); ?>