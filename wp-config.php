<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'atec_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6E{ES!7H;Pund-B30v{x VYn}ND0j13/z&Duy~`yA{G1OB@z!`&G-xd(^D,!!DAf' );
define( 'SECURE_AUTH_KEY',  '` 8zaDvdU/ZG5x/#1]Bw6C6cX,s)_s1)9i R08d qq$qjz$1crRx_JltX~ YQ?6.' );
define( 'LOGGED_IN_KEY',    'Tz^SwXQ%JA)Ckqj]P+]p(Bh).s!!a-[nBoA==.n zZPc#*+0t7Dv]D&zTSUP|h6H' );
define( 'NONCE_KEY',        '5PW.s&;rcBm!Kd;7jWQA?Eo!@m+NOH;HT,}E04NPMWgT=~20CO#4CS1f4.ecjbWp' );
define( 'AUTH_SALT',        '~TH_<Pb*~+BcMVD9Uvy]j3hSB<Dt|QSF7c]t>&yNVTUw`%LFE9A@urewYNT9jjWX' );
define( 'SECURE_AUTH_SALT', '#XT}jc-[Yw;hYazJz83L|4lO<-_d-yO)7vIviu7%mBA|)*7c&[;?JlFegvE%*Oq&' );
define( 'LOGGED_IN_SALT',   '9.;uN5RJWsfg*1Ji+*hef{21):.=wo7A#}i.L$g[pMOq  OzQTG#-FD=ad%NDru&' );
define( 'NONCE_SALT',       '8/S4#xG=s{mRWmO?H*vo,gRa5L95U}HXLS;T7Q|pYj6c9)T{[Sfbk.$2x8v&AIYB' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'atec_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
